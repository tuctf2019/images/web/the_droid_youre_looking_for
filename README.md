# The Droid You're Looking For -- Web -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/web/the_droid_youre_looking_for)

## Chal Info

Desc: `Sometimes you need a little Star Wars in your life to come across the flag`

Hints:

* Only our Google overlords can see the page made for them

Flag: `TUCTF{463nt_6006l3_r3p0rt1n6_4_r0b0t}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/the_droid_youre_looking_for)

Ports: 80

Example usage:

```bash
docker run -d -p 127.0.0.1:8080:80 asciioverflow/the_droid_youre_looking_for:tuctf2019
```
